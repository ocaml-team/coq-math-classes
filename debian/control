Source: coq-math-classes
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Julien Puydt <jpuydt@debian.org>
Section: ocaml
Priority: optional
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: coq,
               debhelper-compat (= 13),
               dh-coq,
               dh-ocaml,
               libcoq-core-ocaml-dev, libcoq-bignums,
               libcoq-stdlib
Vcs-Browser: https://salsa.debian.org/ocaml-team/coq-math-classes
Vcs-Git: https://salsa.debian.org/ocaml-team/coq-math-classes.git
Homepage: https://github.com/coq-community/math-classes

Package: libcoq-math-classes
Architecture: any
Depends: ${coq:Depends}, ${misc:Depends}
Provides: ${coq:Provides}
Description: Abstract interfaces for mathematical structures for Coq
 This library provides abstract interfaces for mathematical
 structures for Coq, such as:
  - algebraic hierarchy (groups, rings, fields, ...)
  - relations, orders, ...
  - Categories, functors, universal algebra, ...
  - Numbers: N, Z, Q, ...
  - Operations (shift, power, abs, ...).
 .
 Coq is a proof assistant for higher-order logic.
